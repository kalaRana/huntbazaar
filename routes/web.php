<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
    // return view('welcome');
});

Route::namespace('App\Http\Controllers')->group(function () { 

    Route::post('create', 'UserAuthController@create')->name('auth.create');
    Route::post('logincheck', 'UserAuthController@loginAction')->name('auth.logincheck');
    Route::get('logout', 'UserAuthController@logout')->name('logout');

    Route::middleware(['isLoggin'])->group( function (){
        Route::get('login', 'UserAuthController@login')->name('login');
        Route::get('register', 'UserAuthController@register');

    });
    Route::get('dashboard', 'DashboardController@index')->name('dashboard')->middleware((['isLogged','isLoggin']));

    Route::middleware(['isLogged'])->group( function (){
        Route::get('profile', 'UserAuthController@profile')->name('profile');

    });

    Route::post('/sendInvitation', 'DashboardController@sendInvitation')->name('sendInvitation');
    Route::post('/submit_daftar', 'DashboardController@daftar')->name('submit_daftar');

    Route::get('register_form/{email_id}', 'DashboardController@form')->name('register_form')->middleware('isRegister');
    Route::post('dataEmail', 'DashboardController@dataEmail')->name('dataEmail');
    Route::post('setDate', 'DashboardController@setDate')->name('setDate');

});
