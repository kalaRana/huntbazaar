

## About Project

Project ini menggunakan Laravel 8

Cara instalasi

    - Clone di sini git clone https://kalaRana@bitbucket.org/kalaRana/huntbazaar.git menggunakan vs code atau gitbash

    - Masuk ke directory project lalu jalankan perintah 
            * composer install
            * php artisan key:generate
            * php artisan optimize  

    - Buat configurasi database dan email account di .env

    - Masuk ke directory project lagi lalu jalankan perintah php artisan migrate --seeds

    - Setelah proses migrasi selesai jalan kan perintah php artisan serve

    - Login sebagai admin ( username: admin@gmail.com password : 123456  )
    
    - Jika menggunakan email schedule pastikan untuk menjalankan perintah : php artisan queue:work


Terima Kasih Aplikasi sudah dapat digunakan

