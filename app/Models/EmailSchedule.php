<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailSchedule extends Model
{
    use HasFactory;
    protected $table = 'email_schedules';
    protected $fillable = ['time_to_email','email_id'];

    public function pendaftaran()
    {
    	return $this->belongsTo(Pendaftaran::class);
    }
}
