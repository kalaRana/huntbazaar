<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    use HasFactory;

    protected $table = 'pendaftarans';
    // protected $fillable = ['id','email','name','bod','gender','design_favorite[]','isRead','status','user_admin','kode_registrasi'];
    protected $casts = [
        'design_favorite' => 'array'
    ];


    public function mailSchedule()
    {
    	return $this->hasOne(EmailSchedule::class);
    }
}
