<?php

namespace App\Listeners;

use App\Mail\ThankYou;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class ThankYouListener implements ShouldQueue
{
 

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        sleep(3600);

        Mail::to($event->details['email'])->send(new ThankYou());

     
    }
}
