<?php

namespace App\Http\Controllers;

use App\Events\ThankYouEvent;
use App\Mail\HuntBazaar;
use App\Models\EmailSchedule;
use App\Models\Pendaftaran;
use Illuminate\Http\Request;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use DB;


class DashboardController extends Controller
{
    function index()
    {

        if(session()->has('LoggedUser')){
            
            $data['user'] = User::where('id', session('LoggedUser'))->first();
            $data['emailSent'] = Pendaftaran::whereNotNull('email')->count();
            $data['emailRead'] = Pendaftaran::where('isRead', 'read')->count();
            $data['registered'] = Pendaftaran::where('status', 'register')->count();
        }
        return view('dashboard', $data);
    }

    public function sendInvitation(Request $req)
    {
        if(session()->has('LoggedUser')){
            
            $user = User::where('id', session('LoggedUser'))->first();
        }

        $email = Pendaftaran::where('email', $req->email_id)->count();

        if($email > 0 ){

            $data = [
                'stat' => '2',
                'message' => 'Undangan sudah dikirim  ke email ini'. $req->emil_id
            ];
        } else {

        $encript = Crypt::encrypt($req->email_id);

        try {   
        $details = [

                'title' => 'Hunt Bazaar !!',
                'body' => 'Follow this link for registration',
                'url_link' => route('register_form',$encript)
        ];
        Mail::to($req->email_id)->send(new HuntBazaar($details));

        $data = [
            'stat' => '-1',
            'message' => 'Email berhasil dikirim'
        ];

        } catch(\Exception $e){
            $data = [
                'stat' => '1',
                'message' => substr($e->getMessage() ,0, 52)
            ];
        }
        }
        if($data['stat'] == '-1'){

            $invitation = new Pendaftaran();
            $invitation->email = $req->email_id;
            $invitation->user_admin = $user->email;
            $email  = $invitation->save();
        }

        return $data;
    }

    public function form($email_id)
    {
        $data['time_set'] = DB::table('table_time_setting')->orderBy('id', 'desc')->first();

        $timeEnd = $data['time_set']->time_set;

        if (Carbon::now()->greaterThan($timeEnd)){
            return view('404')->with('fail', 'URL ini sudah expired');
        }
        $email = Crypt::decrypt($email_id);

        Pendaftaran::where('email', $email)->update(['isRead' => 'read']);

        $data['design_favorite'] = DB::table('table_designer_favorite')->select('nama')->get();

        $data['count'] = Pendaftaran::where('email', $email)->first();

        $data['time_set'] = DB::table('table_time_setting')->orderBy('id', 'desc')->first();

        return view('registrasi', $data);

    }
    public function daftar(Request  $req)
    {
        $req->validate([
                    'name_txt' => 'required|min:4',
                    'tgl_lahir_txt' => 'required',
                    'gender_txt' => 'required',
                    'design_txt' => 'required'
                ]);

        
        $email = $req->email_txt;
        $code = 'HUNT'. date('dmY', strtotime($req->tgl_lahir_txt)) . date('YmdHis');
        try {
                $data = [
                    'email' => $req->email_txt,
                    'name' => $req->name_txt,
                    'bod' => $req->tgl_lahir_txt,
                    'gender' => $req->gender_txt,
                    'design_favorite' => implode(',',$req->design_txt),
                    'status' => 'register',
                    'kode_registrasi' => $code
                ];
                $req->session()->put('memberInfo', $req->email_txt );
    
                $daftar = Pendaftaran::where('email', $req->email_txt)->update($data);

                $details = [
                    'email'=>  $req->email_txt,
                    'body' => 'Dear Bapa/Ibu ' . $req->name_txt .' Terima Kasih sudah mendaftar kode registrasi anda <b>'. $code . '</b>',
                    'title' => 'Hunt Bazaar'
                ];
                event(new ThankYouEvent($details));

            return redirect()->back()->with('success', 'Registration Completed!');

          } catch(\Exception $e){
            return redirect()->back()->with('fail', 'Registration failed!');
          }
    }

    public function dataEmail()
    {
        $data = DB::table('pendaftarans')->select('*')->whereNotNull('name')->get();
        return json_encode($data);
    }
    public function setDate(Request $req)
    {
        DB::beginTransaction();
        try {
            
            DB::table('table_time_setting')->insert([
                'time_set' => $req->tgl_set
            ]);
        DB::commit();
        $data = [
            'stat' => '-1',
            'message' => 'Tanggal berhasil di setting!'
        ];
        }  catch(\Exception $e){
            $data = [
                'stat' => '1',
                'message' => substr($e->getMessage() ,0, 52)
            ];
          }
        return $data;
    }

}
