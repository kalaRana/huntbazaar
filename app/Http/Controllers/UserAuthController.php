<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserAuthController extends Controller
{
    
    function login()
    {
        return view('login');
    }

    function register()
    {
        return view('register');
    }

    function create(Request $req)
    {
        //validate

        $req->validate([
                    'name' => 'required|min:4',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|min:5' 
                ]);

        // save to database
        
            $user = new User;
            $user->name = $req->name;
            $user->email = $req->email;
            $user->password = Hash::make($req->password);
            $data  = $user->save();

        if($data){        
       
            return redirect()->route('login')->with('success', 'User created successfuly!');

        } else {

            return back()->with('fail', 'Somenthing went wrong!');
        }

    }

    function loginAction(Request $req)
    {
        $req->validate([
            'email' => 'required|email',
            'password' => 'required|min:5' 
        ]);

        $user = User::where('email', $req->email)->first();

        if($user){
            if(Hash::check($req->password, $user->password)){

                $req->session()->put('LoggedUser',$user->id );
                
                return redirect()->route('dashboard');

            } else {
                return back()->with('fail', 'Wrong  password!');
            }
        } else {

            return back()->with('fail', 'User Email not Found!');
        }

    }
    function profile() {

        if(session()->has('LoggedUser')){
            
            $user = User::where('id', session('LoggedUser'))->first();

            $data = [
                'LoggedUserInfo' => $user
            ];
        }

        return view('profile', $data);
    }

    function logout()
    {
        if(session()->has('LoggedUser')){
            session()->pull('LoggedUser');
            return redirect()->route('login');
        }
    }
}
