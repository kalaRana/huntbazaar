<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class alreadyRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!session()->has('memberInfo') && (url('regiter_form') == $request->url() || url('regiter_form/{email_id}') == $request->url())) {
            return back()->with('fail', 'You has expired!');
        }
        return $next($request);
    }
}
