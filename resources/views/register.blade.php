@extends('layouts.layouts')
@section('title', 'Register Admin')
@section('content')

        <div class="col-12 col-md-6 offset-xl-2 offset-md-1 order-md-2 mb-5 mb-md-0">

          <!-- Image -->
          <div class="text-center">
            <img src="assets/img/illustrations/happiness.svg" alt="..." class="img-fluid">
          </div>

        </div>
        <div class="col-12 col-md-5 col-xl-4 order-md-1 my-5">
          
          <!-- Heading -->
          <h1 class="display-4 text-center mb-3">
            Sign up
          </h1>
          
          <!-- Subheading -->
          <p class="text-muted text-center mb-5">
            Free access to our dashboard.
          </p>
          <div class="result">
            @if(Session::get('fail'))
                <div class="alert alert-danger">
                    {{ Session::get('fail')}}
                </div>
            @endif
          </div>

          <!-- Form -->
          <form action="{{ route('auth.create') }}" method="post">
            @csrf
            <!-- Email address -->

            
            <div class="form-group">

              <!-- Label -->
              <label>
                Name
              </label>

              <!-- Input -->
              <input type="text" class="form-control" placeholder="your name" name="name" value="{{ old('name') }}" >

             
              <span style="font-style: italic; font-size: 12px; color: red;">@error('name') {{ $message }} @enderror</span>
            </div>

            <!-- Email address -->

            
            <div class="form-group">

              <!-- Label -->
              <label>
                Email Address
              </label>

              <!-- Input -->
              <input type="email" class="form-control" placeholder="name@address.com" name="email" value="{{ old('email') }}" >

             
              <span style="font-style: italic; font-size: 12px; color: red;">@error('email') {{ $message }} @enderror</span>
            </div>

            <!-- Password -->
            <div class="form-group">

              <!-- Label -->
              <label>
                Password
              </label>

              <!-- Input group -->
              <div class="input-group input-group-merge">

                <!-- Input -->
                <input type="password" class="form-control form-control-appended" placeholder="Enter your password" name="password">

                <!-- Icon -->
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="fe fe-eye"></i>
                  </span>
                </div>
                
              </div>
              <span style="font-style: italic; font-size: 12px; color: red;">@error('password') {{ $message }} @enderror</span>
            </div>

            <!-- Submit -->
            <button class="btn btn-lg btn-block btn-primary mb-3">
              Sign up
            </button>

            <!-- Link -->
            <div class="text-center">
              <small class="text-muted text-center">
                Already have an account? <a href="{{url('login')}}">Log in</a>.
              </small>
            </div>

          </form>

        </div>

    
@endsection()