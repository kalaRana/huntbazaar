@extends('layouts.layouts')
@section('title', 'Dashboard Admin')
@section('content')


      <!-- HEADER -->
<div class="header" style="width: 100%; margin-top: 0px !important" id="myHeader">
    <div class="container-fluid">
  <!-- Body -->
    <div class="header-body">
        <div class="row align-items-end">
              <div class="col">
                <h1 class="header-title"> Dashboard </h1>
              </div>

           
              <div class="col-auto">
                
              <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ $user->name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                              <a class="dropdown-item" href="{{route('profile') }}">Profile</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                            </div>
              </div>


              </div>
        </div>
    </div> <!-- / .header-body -->

    </div>
</div> <!-- / .header -->

      <!-- CARDS -->
<div class="container-fluid">

    <div class="row">
        <div class="col-12 col-lg-6 col-xl">
            <!-- Value  -->
            <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                <div class="col">

                    <!-- Title -->
                    <h6 class="text-uppercase text-muted mb-2">
                    Email Terkirim
                    </h6>

                    <!-- Heading -->
                    <span class="h2 mb-0">{{ $emailSent }}
                    </span>

                    <!-- Badge -->
                    <span class="badge badge-soft-success mt-n1">
                    email
                    </span>
                </div>
                <div class="col-auto">


                </div>
                </div> <!-- / .row -->
            </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 col-xl">
            <!-- Value  -->
            <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                <div class="col">

                    <!-- Title -->
                    <h6 class="text-uppercase text-muted mb-2">
                    Email Di Baca
                    </h6>

                    <!-- Heading -->
                    <span class="h2 mb-0">
                    {{ $emailRead }}
                    </span>

                    <!-- Badge -->
                    <span class="badge badge-soft-success mt-n1">
                    email
                    </span>
                </div>
           
                </div> <!-- / .row -->
            </div>
            </div>
        </div>

        <div class="col-12 col-lg-6 col-xl">
            <!-- Value  -->
            <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                <div class="col">

                    <!-- Title -->
                    <h6 class="text-uppercase text-muted mb-2">
                    Sudah Registrasi
                    </h6>

                    <!-- Heading -->
                    <span class="h2 mb-0">
                    {{ $registered }}
                    </span>

                    <!-- Badge -->
                    <span class="badge badge-soft-success mt-n1">
                    peserta
                    </span>
                </div>
              
                </div> <!-- / .row -->
            </div>
            </div>
        </div>

       
    </div>

    <div class="row">

      

        <div class="col-12">

            <div class="row mb-4 auto" >

            <div class="col-auto" style="width: 30%">

                <input type ="email" name="email_invitation" id="email_invitation" class="form-control" placeholder="Ketik email tujuan..."> 
            </div>
            <div class="col-auto">
                    <button class="btn btn-primary float-right mr-0" type="button" onClick="sendInvitation();">
                    <span class="fe fe-send mr-4"></span>
                    Kirim Link Undangan
                    </button>
            </div>

            <div class="form-group">
            <input type="date" class="form-control" id="tgl_bazaar" name="tgl_bazaar">
                <!-- <input type="text" class="form-control" placeholder="2020-01-07" data-toggle="flatpickr" name="tgl_bazaar" id="tgl_bazaar" value="{{ old('tgl_bazaar') }}"> -->
                <!-- <span style="font-style: italic; font-size: 12px; color: red;">@error('tgl_lahir_txt') {{ $message }} @enderror</span> -->
            </div>
            <div class="col-auto">
                <button class="btn btn-primary float-right mr-0" type="button" onClick="savetgglBazar();">
                <span class="fe fe-time mr-4"></span>
                Set Tanggal Bazzar
                </button>
            </div>
            </div>
            <h2 class="mb-3">
                Daftar Member Undangan
              </h2>
                  <!-- Table -->
                  <div class="table-responsive">
                    <table id="tbl-email" class="table table-sm table-nowrap mb-0"  style="width:100%">
                    </table>
                  </div>

        </div>

    </div>


</div>

@endsection

@section('script')
<script type="text/javascript">
var table;
(function($) {
$(document).ready(function() {
    

    table = $('#tbl-email').DataTable({

      
        ajax: {
        url: "{{route('dataEmail') }}",
        type:"POST",
        dataSrc: "",
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        data : {
        type:1,
            "_token": "{{ csrf_token() }}",
            }
        },
        destroy:true,
        searching: false,
        info:false,
        fixedHeader: false,
        responsive:false,
        bAutoWidth: false,
        pageLength : 10,
        sScrollX: true,
        sScrollXInner: "100%",
        bScrollCollapse: true,
        oLanguage: {
            oPaginate: {
                sPrevious: "<<",
                sNext : ">>"
            }
        },
        columnDefs: [
            { width: "15px", "targets": [0]}
            
            ],
        columns: [
            { data:'id', title:'No', className: 'dt-td-center'},
            { data:'name', title:'Nama'},
            { data:'email', title:'Email'},
            { data:'gender', title:'Gender'},
            { data:'design_favorite', title:'Desingner Favorite'},
            // { 
            //     data:"id",
            //     title:'Action',
            //     render: function(data, type, full, meta){
            //         return '<a href="" class="badge-soft-success">hapus</a>'+
            //                 '&nbsp;<a href=""class="badge-soft-success">edit</a>'+
            //                 '&nbsp;<a href=""class="badge-soft-success">resend</a>';
            //     }
            // },
            
            ],
        });

        table.on('order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            table.cell(cell).invalidate('dom');
            });
        }).draw();
    });
})(jQuery);

function sendInvitation(){

    if($('#email_invitation').val() == ''){
        toastr['warning']('Email harus diisi!');
        return false;
    } 
    // alert($('#email_invitation').val());
    $.ajax({
              type  : 'POST',
              url   : "/sendInvitation",
              headers : {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
              data  : { 
                    "_token": "{{ csrf_token() }}",
                    email_id : $('#email_invitation').val(),
               },
              success  : function(data){
                console.log(data);
                if(data.stat === '-1'){
                    toastr['success'](data.message);
                    location.reload();
                    $('#tbl-email').ajax().reload();
                
                } else {
                    toastr['error'](data.message);
                }
                
              },
              error: function(error) {
                toastr['error']('Gagal koneksi, silakan coba lagi!');
                }
            });
}

function savetgglBazar(){
    // console.log($('#tgl_bazaar').val());
    if($('#tgl_bazaar').val() === ''){
        toastr['warning']('Tanggal harus di set!');
        return false;
    }
    $.ajax({
        type  : 'POST',
        url   : "/setDate",
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data  : { 
                "_token": "{{ csrf_token() }}",
                tgl_set : $('#tgl_bazaar').val(),
               },
              success  : function(data){
                console.log(data);
                if(data.stat === '-1'){

                    toastr['success'](data.message);
                    location.reload();
                
                } else {
                    toastr['error'](data.message);
                }
                
              },
              error: function(error) {
                toastr['error']('Gagal koneksi, silakan coba lagi!');
                }
            });
}
</script>
@endsection

   
