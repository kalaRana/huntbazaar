        <select class="custom-select" data-toggle="select">
        <option>My first option</option>
        <option>Another option</option>
        <option>Third option is here</option>
        </select>

        <select class="custom-select" data-toggle="select" data-options='{"minimum-results-for-search": -1}'>
        <option data-avatar-src="../assets/img/avatars/profiles/avatar-1.jpg">
            Dianna Smiley
        </option>
        <option data-avatar-src="../assets/img/avatars/profiles/avatar-2.jpg">
            Ab Hadley
        </option>
        <option data-avatar-src="../assets/img/avatars/profiles/avatar-3.jpg">
            Adolfo Hess
        </option>
        <option data-avatar-src="../assets/img/avatars/profiles/avatar-4.jpg">
            Daniela Dewitt
        </option>
        </select>

        <select class="form-control" data-toggle="select" multiple>
        <option>CSS</option>
        <option>HTML</option>
        <option>JavaScript</option>
        <option>Bootstrap</option>
        </select>

        <select class="form-control form-control-lg" data-toggle="select" multiple>
        <option>CSS</option>
        <option>HTML</option>
        <option>JavaScript</option>
        <option>Bootstrap</option>
        </select>

        <select class="custom-select custom-select-sm" data-toggle="select">
        <option>My first option</option>
        <option>Another option</option>
        <option>Third option is here</option>
        </select>

        <select class="form-control is-valid" data-toggle="select" data-options='{"placeholder": "Valid input"}' multiple>
        <option>CSS</option>
        <option>HTML</option>
        <option>JavaScript</option>
        <option>Bootstrap</option>
        </select>

        <select class="custom-select is-invalid" data-toggle="select" data-options='{"placeholder": "Invalid input"}'>
        <option></option>
        <option>My first option</option>
        <option>Another option</option>
        <option>Third option is here</option>
        </select>