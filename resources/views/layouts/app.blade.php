<!DOCTYPE html>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc." />

    <!-- Libs CSS -->
    <link rel="stylesheet" href="assets/fonts/feather/feather.css" />
    <link rel="stylesheet" href="assets/libs/flatpickr/dist/flatpickr.min.css" />
    <link rel="stylesheet" href="assets/libs/quill/dist/quill.core.css" />
    <link rel="stylesheet" href="assets/libs/highlightjs/styles/vs2015.css" />

    <!-- Map -->
    <link href="../api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css" rel="stylesheet" />

    <!-- Theme CSS -->
      
    <link rel="stylesheet" href="assets/css/theme.min.css" id="stylesheetLight">
    <link rel="stylesheet" href="assets/css/theme-dark.min.css" id="stylesheetDark">

    <style>
      body {
        display: none;
      }

      #myHeader {
        position: fixed;
        top:0;
        height: auto !important;
        width: 100% !important;
        z-index: 100;
    }

    </style>
    
    <!-- Title -->
    <title>@yield('title')</title>

  </head>
  <body>
    <div class="main-content">

      <!-- HEADER -->
      <div class="header" style="width: 100%; margin-top: 0px !important" id="myHeader">
    <div class="container-fluid">
  <!-- Body -->
    <div class="header-body">
        <div class="row align-items-end">
              <div class="col">
                <h1 class="header-title"> Dashboard </h1>
              </div>
           
              <div class="col-auto">
                
              <div class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{Session::get('name')}}
                  </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                              <a class="dropdown-item" href="{{route('profile') }}">Profile</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#!">Logout</a>
                            </div>
              </div>


              </div>
        </div>
    </div> <!-- / .header-body -->

    </div>
</div> <!-- / .header -->


      @yield('content')
    </div>
<script src="assets/libs/jquery/dist/jquery.min.js"></script>
<script src="assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="assets/libs/%40shopify/draggable/lib/es5/draggable.bundle.legacy.js"></script>
<script src="assets/libs/autosize/dist/autosize.min.js"></script>
<script src="assets/libs/chart.js/dist/Chart.min.js"></script>
<script src="assets/libs/dropzone/dist/min/dropzone.min.js"></script>
<script src="assets/libs/flatpickr/dist/flatpickr.min.js"></script>
<script src="assets/libs/highlightjs/highlight.pack.min.js"></script>
<script src="assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="assets/libs/list.js/dist/list.min.js"></script>
<script src="assets/libs/quill/dist/quill.min.js"></script>
<script src="assets/libs/select2/dist/js/select2.full.min.js"></script>
<script src="assets/libs/chart.js/Chart.extension.js"></script>

<script >
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
</body>
</html>