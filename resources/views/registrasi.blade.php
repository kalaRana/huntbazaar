<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc." />

    <!-- Libs CSS -->
    <link rel="stylesheet" href="{{asset('assets/fonts/feather/feather.css') }}" />
    <link rel="stylesheet" href="{{asset('assets/libs/flatpickr/dist/flatpickr.min.css') }}" />
    <link rel="stylesheet" href="{{asset('assets/libs/quill/dist/quill.core.css') }}" />
    <link rel="stylesheet" href="{{asset('assets/libs/highlightjs/styles/vs2015.css') }}" />

    <!-- Map -->
    <!-- <link href="https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css') }}" rel="stylesheet" /> -->

    <!-- Theme CSS -->
      
    <link rel="stylesheet" href="{{asset('assets/css/theme.min.css') }}" id="stylesheetLight">
    <link rel="stylesheet" href="{{asset('assets/css/theme-dark.min.css') }}" id="stylesheetDark">

    <style>
      /* body {
        display: none;
      } */

    </style>
    
    <!-- Title -->
    <title>Form Pendaftaran</title>

  
  </head>

  <div class="main-content">
        <input type="hidden" value="{{ date('M d, Y H:i:s', strtotime($time_set->time_set)) }}" id="timeUp">
        <div class="container-fluid mt-4">
            <div class="row">
                <div class="col-10 m-auto ">
                <div class="result">
                    @if(Session::get('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success')}}
                        </div>
                    @endif
                </div>

            @if($count->status == 'register')
                <div class="card" id="countDown">
                    <div class="card-body">

                    <center>  
                    <div class="btn-group btn-group-lg mr-2 mb-3" role="group" aria-label="First group" id="countDown">
                        <button type="button" class="btn btn-warning" id="hari">1</button>
                        <button type="button" class="btn btn-success">Hari</button>
                        <button type="button" class="btn btn-warning"id="jam">2</button>
                        <button type="button" class="btn btn-success">Jam</button>
                        <button type="button" class="btn btn-warning"id="menit">3</button>
                        <button type="button" class="btn btn-success">Menit</button>
                        <button type="button" class="btn btn-warning"id="detik">4</button>
                        </div>
                        </div>
                    </center>
                    <div class="card-body">
                    <center>
                    <button type="button" class="btn btn-outline-success mb-2" style="">Kode Registrasi Anda : {{ $count->kode_registrasi }}</button>
                    </center>
                    </div>
                </div>
                <!-- Card -->
                @else 
                <h1>FORMULIR PENDAFTARAN</h1>
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('submit_daftar') }}"  method="post">
                            @csrf

                        <input type="hidden" value="{{ Carbon\Carbon::now()->format('H:i:s') }}" name="time_to_email">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" readonly value="{{ $count->email }}" name="email_txt">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Lengkap</label>
                            <input type="text" class="form-control" id="exampleInputName" placeholder="Enter Name" name="name_txt" value="{{ old('name_txt') }}">
                            <span style="font-style: italic; font-size: 12px; color: red;">@error('name_txt') {{ $message }} @enderror</span>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputDOB">Tanggal Lahir</label>
                            <input type="text" class="form-control" placeholder="2020-01-07" data-toggle="flatpickr" name="tgl_lahir_txt" value="{{ old('tgl_lahir_txt') }}">
                            <span style="font-style: italic; font-size: 12px; color: red;">@error('tgl_lahir_txt') {{ $message }} @enderror</span>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputGender">Gender</label>
                            <select class="custom-select" data-toggle="select" name="gender_txt">
                            <option value="Pria">Pria </option>
                            <option value="Wanita">Wanita</option>
                            </select>
                            <span style="font-style: italic; font-size: 12px; color: red;">@error('name_txt') {{ $message }} @enderror</span>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputDesign">Design Favorite</label>
                            <select class="form-control form-control-lg" data-toggle="select" multiple name="design_txt[]">
                                @foreach($design_favorite as $df)
                                    <option value="{{ $df->nama}}">{{ $df->nama }}</option>
                                @endforeach
                            </select>
                            <span style="font-style: italic; font-size: 12px; color: red;">@error('design_txt') {{ $message }} @enderror</span>
                        </div>
                        <button type="submit" class="btn btn-primary" style="float: right; width:150px;">DAFTAR</button>

                        </form>
                    </div>
                </div>
                @endif
                </div>
            
            </div> <!-- / .row -->
        </div>
    </div> <!-- / .main-content -->
    <!-- JAVASCRIPT
    ================================================== -->
    <!-- Libs JS -->
    <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/libs/@shopify/draggable/lib/es5/draggable.bundle.legacy.js') }}"></script>
    <script src="{{ asset('assets/libs/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ asset('assets/libs/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/libs/dropzone/dist/min/dropzone.min.js') }}"></script>
    <script src="{{ asset('assets/libs/flatpickr/dist/flatpickr.min.js') }}"></script>
    <script src="{{ asset('assets/libs/highlightjs/highlight.pack.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('assets/libs/list.js/dist/list.min.js') }}"></script>
    <script src="{{ asset('assets/libs/quill/dist/quill.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/libs/chart.js/Chart.extension.js') }}"></script>

    <!-- Map -->
    <!-- <script src='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script> -->

    <!-- Theme JS -->
    <script src="{{ asset('assets/js/theme.min.js') }}"></script>
    <script src="{{ asset('assets/js/dashkit.min.js') }}"></script>


    <script>
        // var countDownDate = new Date("Jan 5, 2022 15:37:25").getTime();
        var countDownDate = new Date($('#timeUp').val()).getTime();

        var x = setInterval(function() {

        var now = new Date().getTime();
            
        var distance = countDownDate - now;
            
        var hari = Math.floor(distance / (1000 * 60 * 60 * 24));
        var jam = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var menit = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var detik = Math.floor((distance % (1000 * 60)) / 1000);
        $('#hari').text(hari);
        $('#jam').text(jam);
        $('#menit').text(menit);
        $('#detik').text(detik);
            
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countDown").innerHTML = "EXPIRED";
        }
        }, 1000);
        </script>


  </body>
</html>