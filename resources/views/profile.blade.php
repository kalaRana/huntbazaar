@extends('layouts.layouts')
@section('title', 'Profil Admin')
@section('content')

      <!-- HEADER -->
      <div class="header" style="width: 100%; margin-top: 0px !important" id="myHeader">
    <div class="container-fluid">
  <!-- Body -->
    <div class="header-body">
        <div class="row align-items-end">
              <div class="col">
                <h1 class="header-title"> <a href="{{route('dashboard') }}"> BACK </a></h1>
              </div>
           
              <div class="col-auto">
                
              <div class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              {{ $LoggedUserInfo->name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                              <a class="dropdown-item" href="{{route('profile') }}">Profile</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                            </div>
              </div>


              </div>
        </div>
    </div> <!-- / .header-body -->

    </div>
</div> <!-- / .header -->

<div class="container-fluid">
        <div class="row">
          <div class="col-12">

            <!-- Files -->
            <div class="card" data-list="{&quot;valueNames&quot;: [&quot;name&quot;]}">
              <div class="card-header">

                <!-- Title -->
                <h4 class="card-header-title">
                Profile Admin
                </h4>
              </div>
          
              <div class="card-body">

                <!-- List -->
                <ul class="list-group list-group-lg list-group-flush list my-n4"><li class="list-group-item">
                    <div class="row align-items-center">
                      <div class="col-auto">

                        <!-- Avatar -->
                        <a href="#!" class="avatar avatar-lg">
                          <img src="assets/img/files/file-1.jpg" alt="..." class="avatar-img rounded">
                        </a>

                      </div>
                      <div class="col ml-n2">

                        <!-- Title -->
                        <h4 class="mb-1 name">
                          <a href="#!">{{ $LoggedUserInfo->name }}</a>
                        </h4>

                        <!-- Text -->
                        <p class="card-text small text-muted mb-1">
                        {{ $LoggedUserInfo->email }}
                        </p>

                        <!-- Time -->
                        <p class="card-text small text-muted">
                          Aktif sejak  <time datetime="2018-01-03">{{ $LoggedUserInfo->created_at }}</time>
                        </p>

                      </div>
                      <div class="col-auto">

                      

                      </div>
                      <div class="col-auto">

                        <!-- Dropdown -->
                        <div class="dropdown">

                          <!-- Toggle -->
                          <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fe fe-more-vertical"></i>
                          </a>

                          <!-- Menu -->
                          <div class="dropdown-menu dropdown-menu-right">
                            <a href="#!" class="dropdown-item">
                              Ganti Password
                            </a>
                          </div>

                        </div>

                      </div>
                    </div> <!-- / .row -->
                  </li></ul>

              </div>
            </div>

          </div>
        </div> <!-- / .row -->
      </div>


@endsection