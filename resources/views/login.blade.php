@extends('layouts.layouts')
@section('title', 'Login Admin')
@section('content')


    <!-- CONTENT
    ================================================== -->
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-6 offset-xl-2 offset-md-1 order-md-2 mb-5 mb-md-0">

          <!-- Image -->
          <div class="text-center">
            <img src="assets/img/illustrations/happiness.svg" alt="..." class="img-fluid">
          </div>

        </div>
        <div class="col-12 col-md-5 col-xl-4 order-md-1 my-5">
          
          <!-- Heading -->
          <h1 class="display-4 text-center mb-3">
            Sign in
          </h1>
          
          <!-- Subheading -->
          <p class="text-muted text-center mb-5">
          <div class="result">
          @if(Session::get('success'))
              <div class="alert alert-success">
                  {{ Session::get('success')}}
              </div>
          @endif

          @if(Session::get('fail'))
              <div class="alert alert-danger">
                  {{ Session::get('fail')}}
              </div>
          @endif
        </div>
          </p>
          
          <!-- Form -->
          <form action="{{ route('auth.logincheck') }}"method="post" >
            @csrf
            <!-- Email address -->
            <div class="form-group">

              <!-- Label -->
              <label>Email Address</label>

              <!-- Input -->
              <input type="email" class="form-control" placeholder="name@address.com" name="email" value="{{ old('email') }}">
              <span style="font-style: italic; font-size: 12px; color: red;">@error('email') {{ $message }} @enderror</span>

            </div>

            <!-- Password -->
            <div class="form-group">

              <div class="row">
                <div class="col">
                      
                  <!-- Label -->
                  <label>Password</label>

                </div>
                <div class="col-auto">
                  
                  <!-- Help text -->
                  <a href="password-reset-illustration.html" class="form-text small text-muted">
                    Forgot password?
                  </a>

                </div>
              </div> <!-- / .row -->

              <!-- Input group -->
              <div class="input-group input-group-merge">

                <!-- Input -->
                <input type="password" class="form-control form-control-appended" placeholder="Enter your password" name="password">

                <!-- Icon -->
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="fe fe-eye"></i>
                  </span>
                </div>
                
              </div>
              <span style="font-style: italic; font-size: 12px; color: red;">@error('password') {{ $message }} @enderror</span>
            </div>

            <!-- Submit -->
            <button class="btn btn-lg btn-block btn-primary mb-3">
              Sign in
            </button>
            </form>
            <!-- Link -->
            <div class="text-center">
              <small class="text-muted text-center">
                Don't have an account yet? <a href="{{url('register')}}">Sign up</a>.
              </small>
            </div>
            
          

        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->


@endsection()
  