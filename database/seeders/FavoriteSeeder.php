<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FavoriteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Adam Lippes'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Adriano Goldschmied'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Alejandro Ingelmo'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Baby Dior'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Catherine Deane'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Christian Louboutin'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Eugenia Kim'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Giorgio Armani'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Gentle Monster'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Gul Hurgel'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Hugo by Hugo Boss'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Iwan Tirta'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Joshua Sanders'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Karen Walker'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Loeffler Randall'
        ]);
        DB::table('table_designer_favorite')->insert([
            'nama' => 'Outdoor Voices'
        ]);
    }


}
