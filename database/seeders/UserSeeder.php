<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use DB;

use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

            'name' => 'HUNT BAZAAR',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456'),

        ]);

        DB::table('table_time_setting')->insert(
            ['time_set' => '2021-02-24 17:39:38']
        );
    }
}
