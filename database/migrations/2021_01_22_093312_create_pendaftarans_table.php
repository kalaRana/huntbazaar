<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('name')->nullable();
            $table->timestamp('bod')->nullable();
            $table->enum('gender', ['Pria', 'Wanita'])->nullable();
            $table->text('design_favorite')->nullable();
            $table->string('isRead')->default('unread');
            $table->string('status')->default('unregister');
            $table->string('user_admin')->nullable();
            $table->string('kode_registrasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftarans');
    }
}
